﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    public float gravityModifier = 1f;
    public float minGroundNormalY = 0.65f;

    protected Vector2 _targetVelocity;
    protected bool _grounded;
    protected Vector2 _groundNormal;
    protected Vector2 _velocity;
    protected const float _minMoveDist = 0.001f;
    protected const float _shellRadius = 0.02f;
    protected Rigidbody2D _rigidbody;
    protected ContactFilter2D _contactFilter;
    protected RaycastHit2D[] _hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> _hitBufferList = new List<RaycastHit2D>(16);

    protected virtual void ComputeVeclocity(){}

    protected virtual void Update()
    {
        _targetVelocity = Vector2.zero;
        ComputeVeclocity();
    }

    void OnEnable()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _contactFilter.useTriggers = false;
        _contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        _contactFilter.useLayerMask = true;
    }

    void FixedUpdate()
    {
        _velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        _velocity.x = _targetVelocity.x;
        _grounded = false;

        Vector2 tDeltaPos = _velocity * Time.deltaTime;

        Vector2 tMoveAlongGround = new Vector2(_groundNormal.y, -_groundNormal.x);
        Vector2 tMove = tMoveAlongGround * tDeltaPos.x;
        Movement(tMove, false);

        tMove = Vector2.up * tDeltaPos.y;
        Movement(tMove, true);
    }

    void Movement(Vector2 aMove, bool aYMovement)
    {
        float tDist = aMove.magnitude;
        if (tDist > _minMoveDist)
        {
            int tCount = _rigidbody.Cast(aMove, _contactFilter, _hitBuffer, tDist + _shellRadius);
            _hitBufferList.Clear();
            for (int i = 0; i < tCount; i++)
            {
                _hitBufferList.Add(_hitBuffer[i]);
            }

            for (int i = 0; i < _hitBufferList.Count; i++)
            {
                Vector2 tCurrentNormal = _hitBufferList[i].normal;
                if (tCurrentNormal.y > minGroundNormalY)
                {
                    _grounded = true;
                    if (aYMovement)
                    {
                        _groundNormal = tCurrentNormal;
                        tCurrentNormal.x = 0;
                    }
                }

                float tProjection = Vector2.Dot(_velocity, tCurrentNormal);
                if (tProjection < 0)
                {
                    _velocity = _velocity - tProjection * tCurrentNormal;
                }

                float tModifiedDistance = _hitBufferList[i].distance - _shellRadius;
                tDist = tModifiedDistance < tDist ? tModifiedDistance : tDist;
            }
        }

        _rigidbody.position = _rigidbody.position + aMove.normalized * tDist;
    }
}
